.DEFAULT_GOAL := build
INTEGRATION ?= true
export INTEGRATION

listen ?= :6060
index_dir ?= ./tmp/indexer

TEST_OPTIONS = -race -timeout 30s

test:
	@echo "Executing tests with INTEGRATION set to '$(INTEGRATION)'"
	go test ./... ${TEST_OPTIONS}
	@echo "[Done]"
.PHONY:test

cover:
	@echo "Executing tests with INTEGRATION set to '$(INTEGRATION)'"
	go test ./... ${TEST_OPTIONS} -cover -coverprofile=tmp/test.coverage
	gcov2lcov -infile=tmp/test.coverage -outfile=tmp/coverage.xml
	@echo "[Done]"
.PHONY:cover

bench:
	go test -bench=. -benchmem -run ^$$ -benchtime=1s gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/file_cleaner
	@echo "[Done]"
.PHONY:bench

build:
	go build $(VERSION_FLAGS) -o bin/gitlab-zoekt-indexer ./cmd/gitlab-zoekt-indexer/
.PHONY:build

build_web:
	go build $(VERSION_FLAGS) -o bin/gitlab-zoekt-webserver ./cmd/gitlab-zoekt-webserver/
.PHONY:build_web

run: build
	./bin/gitlab-zoekt-indexer -index_dir $(index_dir) -listen $(listen)
.PHONY:run

gdk: check-gdk-dir build
	@echo "Running indexer in GDK mode. GDK_DIR is '$(GDK_DIR)'"
	./bin/gitlab-zoekt-indexer \
		-index_dir '$(GDK_DIR)/zoekt-data/development/index' \
		-listen :6080 \
		-gitlab_url 'http://localhost:3000' \
		-self_url 'http://localhost:6080' \
		-search_url 'http://localhost:6090' \
		-secret_path '$(GDK_DIR)/gitlab/.gitlab_shell_secret'
.PHONY:gdk

gdk-test: check-gdk-dir build
	@echo "Running indexer in GDK mode (test env). GDK_DIR is '$(GDK_DIR)'"
	./bin/gitlab-zoekt-indexer \
		-index_dir '$(GDK_DIR)/zoekt-data/test/index' \
		-listen :6060
.PHONY:gdk-test

watch-run: check-watchexec
	watchexec -c -n -r -e go -- make run
.PHONY:watch-run

watch-gdk: check-watchexec
	watchexec -c -n -r -e go -- make gdk
.PHONY:watch-gdk

watch-test: check-watchexec
	watchexec -c -p -n -e go -- make test
.PHONY:watch-test

watch-cover: check-watchexec
	watchexec -c -p -n -e go -- make cover
.PHONY:watch-cover

check-gdk-dir:
ifndef GDK_DIR
	$(error GDK_DIR must be set)
endif
.PHONY:check-gdk-dir

check-watchexec:
ifeq (, $(shell which watchexec))
	$(error "No watchexec installed, consider running brew install watchexec")
endif
.PHONY:check-watchexec

##### =====> Internals <===== #####

DATE             := $(shell date -u '+%Y.%m.%d')
VERSION          := $(shell git describe --tags --always --dirty=".dev")
BUILD_TIME       := $(shell date -u '+%Y-%m-%d %H:%M UTC')
VERSION_FLAGS    := -ldflags='-X "main.Version=$(DATE)-$(VERSION)" -X "main.BuildTime=$(BUILD_TIME)"'
