# gitlab-zoekt-indexer

## Compiling the binary

```
$ make
$ ./bin/gitlab-zoekt-indexer
2023/08/14 11:07:29 Usage: ./bin/gitlab-zoekt-indexer [ --version | --index_dir=<DIR> | --path_prefix=<PREFIX> | --listen=:<PORT> ]
```

## Running indexer in GDK mode

1. Set `GDK_DIR` env variable (for example, `export GDK_DIR="$HOME/projects/gdk"`).
1. Stop GDK indexer if you have it running via `gdk stop gitlab-zoekt-indexer-development`.
1. Execute `make gdk` or `make watch-gdk`. The latter will restart indexer after every change (requires [watchexec](https://github.com/watchexec/watchexec)).

## Running indexer with docker-compose

For trying out zoekt. Not an official installation method.

See [example](example/docker-compose/README.md)

## Running tests

1. Install a [suitable docker client](https://handbook.gitlab.com/handbook/tools-and-tips/mac/#docker-desktop)
1. Add [zoekt-webserver](https://github.com/sourcegraph/zoekt/tree/main/cmd/zoekt-webserver) to your PATH. If you have zoekt enabled in your GDK you can do:
   ```
   export GDK_DIR="$HOME/projects/gdk"
   export PATH="$GDK_DIR/zoekt/bin:$PATH"
   ```
1. Run the dependencies:
   ```
   docker-compose up
   ```
1. Run the tests:
   ```
   # One time
   make test

   # On every change (requires https://github.com/watchexec/watchexec installed)
   make watch-test 
   ```
