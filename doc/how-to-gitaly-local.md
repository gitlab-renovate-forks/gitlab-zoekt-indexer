## Steps to test a zoekt docker image against local gitaly

### Change zoekt client.rb to pass the docker host's IP address

This is necessary so the indexer running in zoekt knows how to connect to the locally running gitaly instance.


```diff
diff --git a/ee/lib/gitlab/search/zoekt/client.rb b/ee/lib/gitlab/search/zoekt/client.rb
index 5e7755bd5dd4..920282dab53e 100644
--- a/ee/lib/gitlab/search/zoekt/client.rb
+++ b/ee/lib/gitlab/search/zoekt/client.rb
@@ -163,6 +163,8 @@ def indexing_payload(project, force:, callback_payload:)
           repository_path = "#{project.repository.disk_path}.git"
           address = connection_info['address']
 
+          address = "tcp://host.docker.internal:2305"
+
           # This code is needed to support relative unix: connection strings. For example, specs
           if address.match?(%r{\Aunix:[^/.]})
             path = address.split('unix:').last

```

### Change praefect settings

Create `praefect.config.toml` if it doesn't exist already by copying over the example:

```
cp $GDK_DIR/gitaly/config.praefect.toml.example $GDK_DIR/gitaly/praefect.config.toml
```

Then change the listen address to be this value

```
listen_addr = "127.0.0.1:2305"
```


### Restart praefect

```
gdk restart praefect
```

### Stop original zoekt indexer

```
gdk stop gitlab-zoekt-indexer-development
```

### Start docker zoekt indexer with desired tag

```
gl_zoekt_tag="v0.6.1-2f517"
docker run -p 6080:6065 --rm -v ${GDK_DIR}/zoekt-data/development/index:/data/index "registry.gitlab.com/gitlab-org/build/cng/gitlab-zoekt-indexer:${gl_zoekt_tag}"
```

### Run indexing via rails console

```rb
flightjs = Project.find(7)
flightjs.repository.update_zoekt_index!
```
