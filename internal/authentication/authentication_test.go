package authentication

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"go.uber.org/goleak"
)

const (
	staticSecret = "foo"
	jwtIssuer    = "gitlab-shell"
	jwtTTL       = time.Minute
)

func defaultStubbedAuth() Auth {
	timeNowFunc := func() time.Time {
		return time.Date(2000, time.January, 19, 1, 2, 3, 4, time.UTC)
	}

	return Auth{
		jwtIssuer:   jwtIssuer,
		jwtTTL:      jwtTTL,
		secretBytes: []byte(staticSecret),
		timeNowFunc: timeNowFunc,
	}
}

func TestGenerateToken(t *testing.T) {
	tokenString, err := defaultStubbedAuth().GenerateJWT()

	require.NoError(t, err)
	expectedToken := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJnaXRsYWItc2hlbGwiLCJleHAiOjk0ODI0Mzc4MywiaWF0Ijo5NDgyNDM3MjN9._A8tB69mW7tXCdzwdVuqI6lPSBtKkAhH0nmbXi3K6tk" // gitleaks:allow
	require.Equal(t, expectedToken, tokenString)
}

func TestMain(m *testing.M) {
	goleak.VerifyTestMain(m)
}
