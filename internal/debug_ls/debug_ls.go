package debug_ls

import (
	"io/fs"
	"log/slog"
	"path/filepath"
	"time"
)

type FileInfo struct {
	Path    string    `json:"path"`
	Size    int64     `json:"size"`
	ModTime time.Time `json:"mod_time"`
}

func ListFiles(rootDir string) ([]FileInfo, error) {
	files := []FileInfo{}

	err := filepath.WalkDir(rootDir, func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}
		if !d.IsDir() {
			info, err := d.Info()
			if err != nil {
				return err
			}
			files = append(files, FileInfo{
				Path:    path,
				Size:    info.Size(),
				ModTime: info.ModTime(),
			})
		}
		return nil
	})

	if err != nil {
		slog.Error("failed to list files", "err", err)
		return nil, err
	}

	return files, nil
}
