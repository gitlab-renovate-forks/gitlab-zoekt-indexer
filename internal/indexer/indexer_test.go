package indexer_test

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/indexer"
	"go.uber.org/goleak"
)

type BoomWriter struct{}

func (w *BoomWriter) Write(ctx context.Context, p *indexer.IndexParams) (*indexer.IndexingResult, error) {
	return nil, errors.New("boom")
}

func TestIndexRepositoryWhenWriterIsNil(t *testing.T) {
	t.Parallel()

	idx := indexer.Indexer{}

	_, err := idx.IndexRepository(context.Background())
	require.EqualError(t, err, "indexing writer cannot be nil")
}

func TestIndexRepositoryFallbackWhenForceReindex(t *testing.T) {
	idx := indexer.Indexer{
		IndexDir:     t.TempDir(),
		ForceReindex: true,
		Writer:       &BoomWriter{},
	}

	expectedErr := errors.New("something happened")

	_, err := indexer.IndexingFailureFallback(&idx, context.Background(), expectedErr)
	require.EqualError(t, err, expectedErr.Error())
}

func TestIndexRepositoryFallbackSetsForceReindex(t *testing.T) {
	idx := indexer.Indexer{
		IndexDir:     t.TempDir(),
		ForceReindex: false,
		Writer:       &BoomWriter{},
	}

	ctx := context.Background()

	_, expectedErr := idx.IndexRepository(ctx)

	require.False(t, idx.ForceReindex)
	result, err := indexer.IndexingFailureFallback(&idx, ctx, expectedErr)
	require.True(t, idx.ForceReindex)
	require.Nil(t, result)
	require.EqualError(t, expectedErr, err.Error())

}

func TestIndexRepositoryOnFailureCallbackWhenInitialized(t *testing.T) {
	t.Parallel()

	onFailureCalled := false

	idx := indexer.Indexer{
		IndexDir:    t.TempDir(),
		Initialized: true,
		Writer:      &BoomWriter{},
		OnRetryableFailure: func(ix *indexer.Indexer, ctx context.Context, err error) (*indexer.IndexingResult, error) {
			onFailureCalled = true
			return &indexer.IndexingResult{
				ModifiedFilesCount: 8675,
				DeletedFilesCount:  309,
			}, nil
		},
	}

	result, err := idx.IndexRepository(context.Background())
	require.True(t, onFailureCalled)
	require.NoError(t, err)
	require.Equal(t, 8675, int(result.ModifiedFilesCount))
	require.Equal(t, 309, int(result.DeletedFilesCount))
}

func TestIndexRepositoryOnFailureCallbackWhenMaxFailures(t *testing.T) {
	t.Parallel()

	idx := indexer.Indexer{
		IndexDir:    t.TempDir(),
		Initialized: true,
		NumFailures: indexer.MAX_FAILURES,
		Writer:      &indexer.DefaultIndexWriter{},
		OnRetryableFailure: func(ix *indexer.Indexer, ctx context.Context, err error) (*indexer.IndexingResult, error) {
			ix.ForceReindex = true
			return nil, nil
		},
	}

	require.False(t, idx.ForceReindex)
	result, err := idx.IndexRepository(context.Background())
	require.False(t, idx.ForceReindex)
	require.Nil(t, result)
	require.Error(t, err)
}

func TestIndexRepositoryOnFailureCallbackWhenNotInitialized(t *testing.T) {
	t.Parallel()

	idx := indexer.Indexer{
		IndexDir:    t.TempDir(),
		Initialized: false,
		Writer:      &indexer.DefaultIndexWriter{},
		OnRetryableFailure: func(ix *indexer.Indexer, ctx context.Context, err error) (*indexer.IndexingResult, error) {
			ix.ForceReindex = true
			return nil, nil
		},
	}

	require.False(t, idx.ForceReindex)
	result, err := idx.IndexRepository(context.Background())
	require.False(t, idx.ForceReindex)
	require.Nil(t, result)
	require.Error(t, err)
}

func TestMain(m *testing.M) {
	// Ignore glog goleaks since it is indirect
	goleak.VerifyTestMain(m, goleak.IgnoreTopFunction("github.com/golang/glog.(*fileSink).flushDaemon"))
}
