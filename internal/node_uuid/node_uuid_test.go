package node_uuid_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/node_uuid"
	"go.uber.org/goleak"
)

func TestGenerateNewUUID(t *testing.T) {
	nodeUUID := node_uuid.NewNodeUUID(t.TempDir())

	uuid, err := nodeUUID.Get()
	require.NoError(t, err)
	require.NotEmpty(t, uuid)
}

func TestGenerateAndGet(t *testing.T) {
	indexDir := t.TempDir()
	nodeUUID := node_uuid.NewNodeUUID(indexDir)
	uuid, err := nodeUUID.Get()
	require.NoError(t, err)
	require.NotEmpty(t, uuid)

	anotherNodeUUID := node_uuid.NewNodeUUID(indexDir)
	secondUUID, err := anotherNodeUUID.Get()
	require.NoError(t, err)

	require.Equal(t, secondUUID, uuid)
}

func TestMain(m *testing.M) {
	goleak.VerifyTestMain(m)
}
