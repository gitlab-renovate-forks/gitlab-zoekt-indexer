package search

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"mime"
	"net/http"
	"sort"
	"strings"
	"sync"
	"time"
)

const (
	defaultSearchTimeout = "60s"
	defaultMaxResults    = uint32(5000)
)

var (
	forwardedHeaders = []string{
		"Authorization",
	}
)

// Search performs a distributed search across multiple Zoekt web servers.
// It takes an HTTP request containing a SearchRequest in its body,
// validates the request, and executes a multi-node search.
//
// The function:
// 1. Decodes the SearchRequest from the request body
// 2. Validates the request parameters
// 3. Executes a multi-node search using the provided ForwardTo connections
// 4. Combines and sorts the results from all nodes
// 5. Returns a SearchResult or an error
//
// If no ForwardTo connections are specified or the search query is empty,
// it returns an error. The function uses a default timeout if none is specified
// in the request.
//
// Returns:
// - *SearchResult: Combined and sorted search results from all zoekt webservers
// - error: Any error encountered that indicates all searches failed

func (searcher *Searcher) Search(r *http.Request) (*SearchResult, error) {
	req, err := NewSearchRequest(r)
	if err != nil {
		return nil, fmt.Errorf("error building search request: %w", err)
	}

	result, err := searcher.multiNodeSearch(r.Context(), req)
	if err != nil {
		return nil, fmt.Errorf("multi node search failed: %w", err)
	}

	return result, nil
}

func NewSearcher(client *http.Client) *Searcher {
	return &Searcher{Client: client}
}

func NewSearchRequest(r *http.Request) (*SearchRequest, error) {
	if t := r.Header.Get("Content-Type"); t != "application/json" {
		return nil, fmt.Errorf("invalid Content-Type: '%s', expected application/json", t)
	}

	body, err := io.ReadAll(r.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read request body: %w", err)
	}

	var req SearchRequest
	err = json.Unmarshal(body, &req) // nolint:musttag
	if err != nil {
		return nil, fmt.Errorf("failed to parse request body: %w", err)
	}

	if len(req.ForwardTo) == 0 {
		return nil, errors.New("no forward-to connections specified")
	}

	if req.ZoektSearchRequest.Query == "" {
		return nil, errors.New("search query is empty")
	}

	if req.TimeoutString == "" {
		req.TimeoutString = defaultSearchTimeout
	}

	timeout, err := time.ParseDuration(req.TimeoutString)
	if err != nil {
		return nil, fmt.Errorf("failed to parse Timeout: %v with error %w", req.TimeoutString, err)
	}

	req.Timeout = timeout

	if req.Options.TotalMaxMatchCount == 0 {
		req.Options.TotalMaxMatchCount = defaultMaxResults
	}

	req.Headers = make(map[string]string)
	req.Headers["Content-Type"] = "application/json"
	req.Headers["Accept"] = "application/json"

	// Forward headers
	for _, header := range forwardedHeaders {
		if values := r.Header.Values(header); len(values) > 0 {
			req.Headers[header] = strings.Join(values, ",")
		}
	}

	return &req, nil
}

// Conn.DoSearch performs a search request on a single Zoekt web server connection
// and decodes the response.
//
// If any errors occur during this process, they are captured in the returned ZoektResponse.
//
// Parameters:
// - r: ZoektSearchRequest containing the search query and options
//
// Returns:
// - ZoektResponse: Contains the search results or any error that occurred

func (searcher *Searcher) DoSearch(ctx context.Context, r *SearchRequest, c *Conn) ZoektResponse {
	jsonBody, err := json.Marshal(ZoektSearchRequest{
		Query:   r.Query,
		Options: r.Options,
		RepoIds: r.RepoIds,
	})
	if err != nil {
		return ZoektResponse{Error: fmt.Errorf("failed to marshal ZoektSearchRequest: %w", err), Endpoint: c.Endpoint}
	}

	// Create a new request
	req, err := http.NewRequestWithContext(ctx, http.MethodPost, c.Endpoint, bytes.NewReader(jsonBody))
	if err != nil {
		return ZoektResponse{Error: err, Endpoint: c.Endpoint}
	}

	// Set headers from the list
	for key, value := range r.Headers {
		req.Header.Set(key, value)
	}

	// Send the request
	resp, err := searcher.Client.Do(req)

	if err != nil {
		return ZoektResponse{Error: err, Endpoint: c.Endpoint}
	}

	defer resp.Body.Close() // nolint:errcheck

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return ZoektResponse{
			Error:    fmt.Errorf("failed to read response body: %w", err),
			Endpoint: c.Endpoint,
		}
	}

	if resp.StatusCode >= 300 {
		return ZoektResponse{
			Error:    fmt.Errorf("HTTP request failed with status code: %d; body: %s", resp.StatusCode, body),
			Endpoint: c.Endpoint,
		}
	}

	mediaType, _, err := mime.ParseMediaType(resp.Header.Get("Content-Type"))
	if err != nil || mediaType != "application/json" {
		return ZoektResponse{
			Error:    fmt.Errorf("unexpected content type: %s, expected application/json", resp.Header.Get("Content-Type")),
			Endpoint: c.Endpoint,
		}
	}

	zoektResp := ZoektResponse{}
	err = json.Unmarshal(body, &zoektResp) // nolint:errchkjson,musttag
	if err != nil {
		return ZoektResponse{
			Error:    err,
			Endpoint: c.Endpoint,
		}
	}

	zoektResp.Endpoint = c.Endpoint

	return zoektResp
}

// multiNodeSearch performs a distributed search across multiple Zoekt web servers.
// It concurrently sends search requests to all specified connections,
// combines the results, sorts them, and handles timeouts and errors.
// Returns a consolidated SearchResult or an error if all searches fail.
func (searcher *Searcher) multiNodeSearch(ctx context.Context, s *SearchRequest) (*SearchResult, error) {
	var wg sync.WaitGroup
	responses := make(chan ZoektResponse, len(s.ForwardTo))

	timeoutCtx, cancel := context.WithTimeout(ctx, s.Timeout)
	defer cancel()

	for i := range s.ForwardTo {
		wg.Add(1)
		go func() {
			defer wg.Done()

			responses <- searcher.DoSearch(timeoutCtx, s, &s.ForwardTo[i])
		}()
	}

	go func() {
		wg.Wait()
		close(responses)
	}()

	result := combineResults(timeoutCtx, responses, s)

	if result.TimedOut && len(result.Result.Files) == 0 {
		return nil, fmt.Errorf("search timed out")
	}

	if len(result.Failures) == len(s.ForwardTo) {
		var errMsgs []error
		for _, failure := range result.Failures {
			errMsgs = append(errMsgs, fmt.Errorf("%s: %s", failure.Endpoint, failure.Error))
		}
		combinedErr := errors.Join(errMsgs...)

		return nil, fmt.Errorf("all searches failed: %w", combinedErr)
	}

	result.sort()

	// Limit the number of results to the max results specified in the request
	if result.Result.FileCount > s.Options.TotalMaxMatchCount {
		result.Result.Files = result.Result.Files[:s.Options.TotalMaxMatchCount]
		result.Result.FileCount = s.Options.TotalMaxMatchCount
	}

	return result, nil
}

// combineResults aggregates search results from multiple Zoekt web servers.
// It processes responses from a channel, combines the results, and handles errors.
// The function stops processing when either all responses are received or a timeout occurs.
// It returns a SearchResult containing combined file matches, counts, and any failures.
func combineResults(ctx context.Context, ch <-chan ZoektResponse, s *SearchRequest) *SearchResult {
	combinedResult := &SearchResult{}
	for {
		select {
		case resp, ok := <-ch:
			if !ok {
				return combinedResult
			}

			if resp.Error != nil {
				combinedResult.Failures = append(combinedResult.Failures, SearchFailure{
					Error:    resp.Error.Error(),
					Endpoint: resp.Endpoint,
				})

				continue
			}

			combinedResult.Result.Files = append(combinedResult.Result.Files, resp.Result.Files...)
			combinedResult.Result.FileCount += uint32(resp.Result.FileCount)       //nolint:gosec
			combinedResult.Result.MatchCount += uint32(resp.Result.MatchCount)     //nolint:gosec
			combinedResult.Result.NgramMatches += uint32(resp.Result.NgramMatches) //nolint:gosec

			// If we have reached the max results, we can stop processing
			if combinedResult.Result.FileCount >= s.Options.TotalMaxMatchCount {
				return combinedResult
			}

		case <-ctx.Done():
			combinedResult.TimedOut = true
			return combinedResult
		}
	}
}

// SearchResult.sort sorts the search results in descending order based on the score of each file match.
// This ensures that the most relevant results are presented at the beginning of the list.
// The sorting is done in-place, modifying the provided SearchResult directly.
func (s *SearchResult) sort() {
	sort.Slice(s.Result.Files, func(i, j int) bool {
		return s.Result.Files[i].Score > s.Result.Files[j].Score
	})
}
