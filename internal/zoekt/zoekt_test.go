package zoekt_test

import (
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/projectpath"
	"gitlab.com/gitlab-org/gitlab-zoekt-indexer/internal/zoekt"
	"go.uber.org/goleak"
)

var (
	ExistingID    uint32 = 10
	NonExistentID uint32 = 20
)

func TestGetCurrentSHA(t *testing.T) {
	dir := filepath.Join(projectpath.Root, "_support/test/shards")
	client := zoekt.NewZoektClient(&zoekt.Options{
		IndexDir: dir,
		ID:       ExistingID,
	})

	result, ok, err := client.GetCurrentSHA()

	require.NoError(t, err)

	expectedSHA := "5f6ffd6461ba03e257d24eed4f1f33a7ee3c2784"
	require.Equal(t, expectedSHA, result, "The two SHA should be the same.")
	require.True(t, ok)

	client = zoekt.NewZoektClient(&zoekt.Options{
		IndexDir: dir,
		ID:       NonExistentID,
	})

	_, ok, err = client.GetCurrentSHA()

	require.NoError(t, err)
	require.False(t, ok)
}

func TestMain(m *testing.M) {
	goleak.VerifyTestMain(m)
}
